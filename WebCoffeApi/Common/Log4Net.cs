﻿
// Type: FasCon.Apis.Common.Log4Net




using System;
using System.IO;
using System.Text;


namespace FasCon.Apis.Common
{
  public static class Log4Net
  {
    public static void writeLog(string logContent)
    {
      string path = AppDomain.CurrentDomain.BaseDirectory + "\\ErrorLog\\";
      if (!Directory.Exists(path))
        Directory.CreateDirectory(path);
      StringBuilder stringBuilder = new StringBuilder();
      string str = "Log_" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt";
      if (File.Exists(path + str))
      {
        StreamReader streamReader = File.OpenText(path + str);
        string end = streamReader.ReadToEnd();
        stringBuilder.AppendLine(end);
        streamReader.Close();
        streamReader.Dispose();
      }
      stringBuilder.AppendLine(logContent);
      StreamWriter text = File.CreateText(path + str);
      text.WriteLine((object) stringBuilder);
      text.Close();
      text.Dispose();
    }

    public static void writeLog(Exception ex)
    {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.AppendLine("==================================");
      stringBuilder1.AppendLine(" Date : " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
      stringBuilder1.AppendLine();
      stringBuilder1.AppendLine(" StackTrace : " + ex.StackTrace);
      stringBuilder1.AppendLine();
      stringBuilder1.AppendLine(" Message: " + ex.Message);
      stringBuilder1.AppendLine();
      stringBuilder1.AppendLine("InnerException : " + (ex.InnerException == null ? string.Empty : ex.InnerException.Message));
      stringBuilder1.AppendLine("==================================");
      string path = AppDomain.CurrentDomain.BaseDirectory + "\\ErrorLog\\";
      if (!Directory.Exists(path))
        Directory.CreateDirectory(path);
      StringBuilder stringBuilder2 = new StringBuilder();
      string str = "Log_" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt";
      if (File.Exists(path + str))
      {
        StreamReader streamReader = File.OpenText(path + str);
        string end = streamReader.ReadToEnd();
        stringBuilder2.AppendLine(end);
        streamReader.Close();
        streamReader.Dispose();
      }
      stringBuilder2.AppendLine(stringBuilder1.ToString());
      StreamWriter text = File.CreateText(path + str);
      text.WriteLine((object) stringBuilder2);
      text.Close();
      text.Dispose();
    }

    public static void RemoveLogFile()
    {
      string path = AppDomain.CurrentDomain.BaseDirectory + "\\ErrorLog\\";
      if (!Directory.Exists(path))
        Directory.CreateDirectory(path);
      foreach (FileInfo file in new DirectoryInfo(path).GetFiles())
      {
        DateTime dateTime = file.LastWriteTime;
        dateTime = dateTime.Date;
        string str = dateTime.ToString("yyyyMMdd");
        dateTime = DateTime.Now;
        dateTime = dateTime.AddDays(-15.0);
        string strB = dateTime.ToString("yyyyMMdd");
        if (str.CompareTo(strB) < 0)
          file.Delete();
      }
    }
  }
}
