using System;
using System.Data.SqlClient;
using FasCon.Apis.Common;
using FasCon.Apis.Models;
using FasCon.Apis.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace FasCon.Apis.Controller
{
    //Đông fixed this controller.
    [ApiController]
    [Route("[controller]")]
    public class CategoryController : ControllerBase
    {
        public CategoryController()
        {
            this._categoryService = new CategoryService();
        }


        //Post: {"CompanyId":"2403221134200464037"}
        [Route("api/Category/search")]
        [HttpPost]
        public ResponseMessageModels Search(CategoryModel searchModel)
        {
            int statusCode = 9;
            return new ResponseMessageModels
            {
                Data = this._categoryService.GetAll(searchModel, out statusCode),
                Status = new StatusCodeModels
                {
                    StatusCode = statusCode,
                    MessageId = CommonData.DisplayMessage(statusCode),
                    Version = "v1"
                }

            };

        }
        //Post: {"Code":"add","Name":"add","Note":"add","CreatedBy":"test","CompanyId":"2403221134200464037"}

        [Route("api/Category/Add")]
        [HttpPost]
        public ResponseMessageModels Add(CategoryModel model)
        {
            int statusCode = 9;
            var re = this._categoryService.Add(model, out statusCode);
            return new ResponseMessageModels
            {
                Data = re,
                Status = new StatusCodeModels
                {
                    StatusCode = statusCode,
                    MessageId = CommonData.DisplayMessage(statusCode),
                    Version = "v1"
                }

            };

        }
        //Post: {"Id":501558,"CompanyId":"2403221134200464037"}
        [Route("api/Category/Delete")]
        [HttpPost]
        public ResponseMessageModels Delete(long CategoryId, string updateBy, string companyId)
        {
            int statusCode = 9;
            var re = this._categoryService.Delete(CategoryId, updateBy, companyId, out statusCode);
            return new ResponseMessageModels
            {
                Data = re,
                Status = new StatusCodeModels
                {
                    StatusCode = statusCode,
                    MessageId = CommonData.DisplayMessage(statusCode),
                    Version = "v1"
                }

            };

        }
        //No need
        [Route("api/Category/GetCategoryById")]
        [HttpGet]
        public ResponseMessageModels GetCategoryById(long id)
        {
            int statusCode = 9;

            return new ResponseMessageModels
            {
                Data = this._categoryService.GetCategoryById(id, out statusCode),
                Status = new StatusCodeModels
                {
                    StatusCode = statusCode,
                    MessageId = CommonData.DisplayMessage(statusCode),
                    Version = "v1"
                }

            };
        }
        //Post: {"Code":"test","Name":"test","Note":"test","UpdatedBy":"test","CompanyId":"2403221134200464037"}
        [Route("api/Category/Save")]
        [HttpPost]
        public ResponseMessageModels Save(CategoryModel model)
        {
            int statusCode = 9;
            var re = this._categoryService.Update(model, out statusCode);
            return new ResponseMessageModels
            {
                Data = re,
                Status = new StatusCodeModels
                {
                    StatusCode = statusCode,
                    MessageId = CommonData.DisplayMessage(statusCode),
                    Version = "v1"
                }

            };
        }
        //Post: {"pageSize":3,"pageIndex":1,"CompanyId":"2403221134200464037"}
        [Route("api/Category/Pagination")]
        [HttpPost]
        public ResponseMessageModels pagination(int pageSize, int pageIndex, string companyId)
        {
            int statusCode = 9;
            return new ResponseMessageModels
            {
                Data = this._categoryService.Pagnition(pageSize, pageIndex, companyId, out statusCode),
                Status = new StatusCodeModels
                {
                    StatusCode = statusCode,
                    MessageId = CommonData.DisplayMessage(statusCode),
                    Version = "v1"
                }

            };

        }

        //Post: {"Code":"L","CompanyId":"2403221134200464037"}
        [Route("api/Category/SearchByCode")]
        [HttpPost]
        public ResponseMessageModels SearchByCode(string codeName, string companyId)
        {
            int statusCode = 9;
            return new ResponseMessageModels
            {
                Data = this._categoryService.SearchByCode(codeName, companyId, out statusCode),
                Status = new StatusCodeModels
                {
                    StatusCode = statusCode,
                    MessageId = CommonData.DisplayMessage(statusCode),
                    Version = "v1"
                }

            };

        }
        private ICategoryService _categoryService;

    }
}
