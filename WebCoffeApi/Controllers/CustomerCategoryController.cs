﻿using System;
using System.Data.SqlClient;
using FasCon.Apis.Common;
using FasCon.Apis.Models;
using FasCon.Apis.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace FasCon.Apis.Controller
{
    //Kha fixed this controller.
    [ApiController]
    [Route("[controller]")]
    public class CustomerCategoryController : ControllerBase
    {
        private ICustomerCategoryService _customerCategoryService;

        public CustomerCategoryController()
        {
            this._customerCategoryService = new CustomerCategoryService();
        }

        //Post: {"CompanyId":"2403221134200464037"}
        [Route("api/CustomerCategory/search")]
        [HttpPost]
        public ResponseMessageModels Search(CustomerCategoryModels searchModel)
        {
            int statusCode = 9;
            return new ResponseMessageModels
            {
                Data = this._customerCategoryService.GetAll(searchModel, out statusCode),
                Status = new StatusCodeModels
                {
                    StatusCode = statusCode,
                    MessageId = CommonData.DisplayMessage(statusCode),
                    Version = "v1"
                }

            };

        }


        //need:"Code":"777","CompanyId":"2403221134200464037"}
        [Route("api/UnitOfMeasure/SearchByCode")]
        [HttpPost]
        public ResponseMessageModels SearchByCode(string codeName, string companyId)
        {
            int statusCode = 9;
            return new ResponseMessageModels
            {
                Data = this._customerCategoryService.SearchByCode(codeName, companyId, out statusCode),
                Status = new StatusCodeModels
                {
                    StatusCode = statusCode,
                    MessageId = CommonData.DisplayMessage(statusCode),
                    Version = "v1"
                }

            };

        }


        //Post: {"Code":"add","Name":"add","Note":"add","CreatedBy":"test","CompanyId":"2403221134200464037"}
        [Route("api/CustomerCategory/Add")]
        [HttpPost]
        public ResponseMessageModels Add(CustomerCategoryModels model)
        {
            int statusCode = 9;
            this._customerCategoryService.Add(model, out statusCode);
            return new ResponseMessageModels
            {
                Data = statusCode == 0 ? "Add success" : "Add failed",
                Status = new StatusCodeModels
                {
                    StatusCode = statusCode,
                    MessageId = CommonData.DisplayMessage(statusCode),
                    Version = "v1"
                }

            };

        }

        //Post: {"Id":555"CompanyId":"2403221134200464037"}
        [Route("api/CustomerCategory/Delete")]
        [HttpPost]
        public ResponseMessageModels Delete(long CustomerId, string updateBy, string companyId)
        {
            int statusCode = 9;
            this._customerCategoryService.Delete(CustomerId, updateBy, companyId, out statusCode);
            return new ResponseMessageModels
            {
                Data = statusCode == 0 ? "Delete success" : "Delete failed",
                Status = new StatusCodeModels
                {
                    StatusCode = statusCode,
                    MessageId = CommonData.DisplayMessage(statusCode),
                    Version = "v1"
                }

            };

        }

        //Get: {"Id":555}
        [Route("api/CustomerCategory/GetCustomerById")]
        [HttpGet]
        public ResponseMessageModels GetCustomerById(long id)
        {
            int statusCode = 9;

            return new ResponseMessageModels
            {
                Data = this._customerCategoryService.GetCustomerCategoryById(id, out statusCode),
                Status = new StatusCodeModels
                {
                    StatusCode = statusCode,
                    MessageId = CommonData.DisplayMessage(statusCode),
                    Version = "v1"
                }

            };
        }

        //Post: {"Code":"test","Name":"test","Note":"test","UpdatedBy":"test","CompanyId":"2403221134200464037"}
        [Route("api/CustomerCategory/Save")]
        [HttpPost]
        public ResponseMessageModels Save(CustomerCategoryModels model)
        {
            int statusCode = 9;
            this._customerCategoryService.Update(model, out statusCode);
            return new ResponseMessageModels
            {
                Data = statusCode == 0 ? "Save success" : "Save failed",
                Status = new StatusCodeModels
                {
                    StatusCode = statusCode,
                    MessageId = CommonData.DisplayMessage(statusCode),
                    Version = "v1"
                }

            };
        }

        //Post: {"pageSize":5,"pageIndex":1,"CompanyId":"2403221134200464037"}
        [Route("api/CustomerCategory/Pagination")]
        [HttpPost]
        public ResponseMessageModels pagination(int pageSize, int pageIndex, string companyId)
        {
            int statusCode = 9;
            return new ResponseMessageModels
            {
                Data = this._customerCategoryService.Pagnition(pageSize, pageIndex, companyId, out statusCode),
                Status = new StatusCodeModels
                {
                    StatusCode = statusCode,
                    MessageId = CommonData.DisplayMessage(statusCode),
                    Version = "v1"
                }

            };

        }
    }
}
