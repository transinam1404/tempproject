using System;
using System.Collections.Generic;
using System.Linq;
using FasCon.Apis.Common;
using FasCon.Apis.Models;
using FasCon.Apis.Services;
using Microsoft.AspNetCore.Mvc;
using WebCoffeApi.Models;
using WebCoffeApi.Services.Implements;

namespace FasCon.Apis.Controller
{

    // Lam Duy chinh sua controler nay
    [ApiController]
    [Route("[controller]")]
    public class SaleAreaController : ControllerBase

    {
        public SaleAreaController()
        {
            this._saleAreaService = new SaleAreaService();
        }


        // post: {"CompanyId":"2403221134200464037", "branchId": "BRA2403221134200784894"}
        [Route("api/SaleArea/search")]
        [HttpPost]
        public ResponseMessageModels Search(SaleAreaModels modelSearch)
        {
            int statusCode = 9;

            return new ResponseMessageModels
            {
                Data = this._saleAreaService.GetList(modelSearch, out statusCode),
                Status = new StatusCodeModels
                {
                    StatusCode = 0,
                    MessageId = CommonData.DisplayMessage(0),
                    Version = "v1"
                }
            };
        }

        [Route("api/SaleArea/GetSaleAreaById")]
        [HttpGet]
        public ResponseMessageModels GetSaleAreaById(long Id)
        {
            int statusCode = 9;
            var saleArea = _saleAreaService.SelectByID(new SaleAreaModels { ID = Id });
            if (saleArea == null)
            {
                statusCode = 9;
                return new ResponseMessageModels
                {
                    Data = null,
                    Status = new StatusCodeModels
                    {
                        StatusCode = statusCode,
                        MessageId = CommonData.DisplayMessage(statusCode),
                        Version = "v1"
                    }
                };
            }
            statusCode = 0;
            return new ResponseMessageModels
            {
                Data = saleArea,
                Status = new StatusCodeModels
                {
                    StatusCode = statusCode,
                    MessageId = CommonData.DisplayMessage(statusCode),
                    Version = "v1"
                }
            };
        }

        // post: {
        //  "id": 0,
        //  "code": "new code",
        //  "name": "new name",
        //  "printer": "not yet",
        //  "arrangement": 0,
        //  "isUsed": 0,
        //  "note": "string",
        //  "branchId": "string",
        //  "companyId": "string",
        //  "userName": "string",
        //  "isDeleted": 0,
        //  "isTinhGio": 0
        //}
        [Route("api/SaleArea/Create")]
        [HttpPost]
        public ResponseMessageModels Create (SaleAreaModels newModel)
        {
            int statusCode = 9;
            if (_saleAreaService.IsCodeExists(newModel.BranchId, newModel.Code))
            {
                ResponseMessageModels responseMessageModels = new ResponseMessageModels();
                responseMessageModels.Status = new StatusCodeModels
                {
                    StatusCode = 5,
                    MessageId = CommonData.DisplayMessage(5),
                    Version = "v1"
                };

                return responseMessageModels;
               

            }else
            {
                var newSaleArea = this._saleAreaService.Insert(newModel);
                statusCode = 0;
                return new ResponseMessageModels
                {
                    Data = newSaleArea,
                    Status = new StatusCodeModels
                    {
                        StatusCode = statusCode,
                        MessageId = CommonData.DisplayMessage(statusCode),
                        Version = "v1"
                    }
                };

            }



        }


        // post :{
        //  "id": 75575,
        //  "code": "KV-004",
        //  "name": "phòng test",
        //  "printer": "Microsoft test PDF",
        //  "arrangement": 0,
        //  "isUsed": 0,
        //  "note": "string",
        //  "branchId": "string",
        //  "companyId": "string",
        //  "userName": "string",
        //  "isDeleted": 0,
        //  "isTinhGio": 0
        //}
        [Route("api/SaleArea/Save")]
        [HttpPost]
        public ResponseMessageModels Save(SaleAreaModels model)
        {
            ResponseMessageModels responseMessageModels = new ResponseMessageModels();
            if (this._saleAreaService.KiemTraTonTaiID(model.CompanyId, model.BranchId, model.ID) == 1)
            {
                responseMessageModels.Data = this._saleAreaService.Update(model);
            }
            else
            {
                if (this._saleAreaService.IsCodeExists(model.BranchId, model.Code))
                {
                    responseMessageModels.Status = new StatusCodeModels
                    {
                        StatusCode = 5,
                        MessageId = CommonData.DisplayMessage(5),
                        Version = "v1"
                    };
                    return responseMessageModels;
                }
                responseMessageModels.Data = this._saleAreaService.Insert(model);
            }
            responseMessageModels.Status = new StatusCodeModels
            {
                StatusCode = 0,
                MessageId = CommonData.DisplayMessage(0),
                Version = "v1"
            };
            return responseMessageModels;
        }


        // delete single item
        // post: {"id": 75575}
        [Route("api/SaleArea/Delete")]
        [HttpPost]
        public ResponseMessageModels Delete (string CompanyId, string BranchId, string UserName, long Id)
        {


            int statusCode = 9;
           

            var result = this._saleAreaService.Delete(CompanyId, BranchId, UserName, Id, out statusCode);
            return new ResponseMessageModels
            {
                Data = result,
                Status = new StatusCodeModels
                {
                    StatusCode = statusCode,
                    MessageId = CommonData.DisplayMessage(statusCode),
                    Version = "v1"
                }
            };

                    
                    
                    
        }


        

        [Route("api/SaleArea/Deletelist")]
        [HttpPost]
        public object DeleteList(List<SaleAreaModels> listmodel)
        {

            ResponseMessageModels responseMessageModels = new ResponseMessageModels();
            // check list of model is null or empty first 
            if (listmodel == null || !listmodel.Any())
            {
                responseMessageModels.Status = new StatusCodeModels
                {
                    StatusCode = 1, // Assuming 1 indicates an error
                    MessageId = "No items to delete.",
                    Version = "v1"
                };
                return responseMessageModels;
            }
            long[] arr = listmodel.Select(sa => sa.ID).ToArray<long>();

            long[] arrAreaId;

            int num = this._saleAreaService.IsCheckDelete(
                listmodel.First().CompanyId,
                listmodel.First().BranchId,
                arr,
                out arrAreaId
                );

            if (num != 0 && arrAreaId.Length != 0)
            {
                string str = string.Join(",", listmodel
                    .Where(t => arrAreaId.Contains(t.ID))
                    .Select(p => p.Code)
                    );

                responseMessageModels.Status = new StatusCodeModels
                {
                    StatusCode = num,
                    MessageId = str + " đã được sử dụng, không thể xóa.",
                    Version = "v1"
                };
                return responseMessageModels;
            }
            string companyId = listmodel.First().CompanyId;
            string branchId = listmodel.First().BranchId;
            string userName = listmodel.First().UserName;



            responseMessageModels.Data = this._saleAreaService.Delete(companyId, branchId, userName, arr);

            responseMessageModels.Status = new StatusCodeModels
            {
                StatusCode = 0,
                MessageId = CommonData.DisplayMessage(0),
                Version = "v1"
            };
            return responseMessageModels;
        }
        /*
            pageSize: The number of records per page.
            pageIndex: The page number to retrieve.
            companyId: The ID of the company to filter records by.
            statusCode: An output parameter indicating the success or failure of the operation.

            //Post: {"pageSize":3,"pageIndex":1,"CompanyId":"2403221134200464037"}
         */
        [Route("api/SaleArea/Pagination")]
        [HttpPost]
        public ResponseMessageModels pagination (int pageSize, int pageIndex, string companyId)
        {
            int statusCode = 9;
            return new ResponseMessageModels
            {

                Data = _saleAreaService.Pagintion(pageSize, pageIndex, companyId, out statusCode),
                Status = new StatusCodeModels
                {
                    StatusCode = 0,
                    MessageId = CommonData.DisplayMessage(0),
                    Version = "v1"
                },
            };
        }



        private ISaleAreaService _saleAreaService;
    }
}
