using System;
using System.Data.SqlClient;
using FasCon.Apis.Common;
using FasCon.Apis.Models;
using FasCon.Apis.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace FasCon.Apis.Controller
{
    //Nam fixed this controller.
    [ApiController]
    [Route("[controller]")]
    public class UnitOfMeasureController : ControllerBase
    {
        public UnitOfMeasureController()
        {
            this._unitOfMeasureService = new UnitOfMeasureService();
        }


        //Post: {"CompanyId":"2403221134200464037"}
        [Route("api/UnitOfMeasure/search")]
        [HttpPost]
        public ResponseMessageModels Search(UnitOfMeasureModel searchModel)
        {
            int statusCode = 9;
            return new ResponseMessageModels
            {
                Data = this._unitOfMeasureService.GetAll(searchModel, out statusCode),
                Status = new StatusCodeModels
                {
                    StatusCode = statusCode,
                    MessageId = CommonData.DisplayMessage(statusCode),
                    Version = "v1"
                }

            };

        }
        //Post: {"Code":"add","Name":"add","Note":"add","CreatedBy":"test","CompanyId":"2403221134200464037"}

        [Route("api/UnitOfMeasure/Add")]
        [HttpPost]
        public ResponseMessageModels Add(UnitOfMeasureModel model)
        {
            int statusCode = 9;
            var re = this._unitOfMeasureService.Add(model, out statusCode);
            return new ResponseMessageModels
            {
                Data = re,
                Status = new StatusCodeModels
                {
                    StatusCode = statusCode,
                    MessageId = CommonData.DisplayMessage(statusCode),
                    Version = "v1"
                }

            };

        }
        //Post: {"Id":501558,"CompanyId":"2403221134200464037"}
        [Route("api/UnitOfMeasure/Delete")]
        [HttpPost]
        public ResponseMessageModels Delete(long measureId, string updateBy, string companyId)
        {
            int statusCode = 9;
            var re = this._unitOfMeasureService.Delete(measureId, updateBy, companyId, out statusCode);
            return new ResponseMessageModels
            {
                Data = re,
                Status = new StatusCodeModels
                {
                    StatusCode = statusCode,
                    MessageId = CommonData.DisplayMessage(statusCode),
                    Version = "v1"
                }

            };

        }
        //No need
        [Route("api/UnitOfMeasure/GetMeasureById")]
        [HttpGet]
        public ResponseMessageModels GetMeasureById(long id)
        {
            int statusCode = 9;

            return new ResponseMessageModels
            {
                Data = this._unitOfMeasureService.GetMeasureById(id, out statusCode),
                Status = new StatusCodeModels
                {
                    StatusCode = statusCode,
                    MessageId = CommonData.DisplayMessage(statusCode),
                    Version = "v1"
                }

            };
        }
        //Post: {"Code":"test","Name":"test","Note":"test","UpdatedBy":"test","CompanyId":"2403221134200464037"}
        [Route("api/UnitOfMeasure/Save")]
        [HttpPost]
        public ResponseMessageModels Save(UnitOfMeasureModel model)
        {
            int statusCode = 9;
            var re = this._unitOfMeasureService.Update(model, out statusCode);
            return new ResponseMessageModels
            {
                Data = re,
                Status = new StatusCodeModels
                {
                    StatusCode = statusCode,
                    MessageId = CommonData.DisplayMessage(statusCode),
                    Version = "v1"
                }

            };
        }
        //Post: {"pageSize":3,"pageIndex":1,"CompanyId":"2403221134200464037"}
        [Route("api/UnitOfMeasure/Pagination")]
        [HttpPost]
        public ResponseMessageModels pagination(int pageSize, int pageIndex, string companyId)
        {
            int statusCode = 9;
            return new ResponseMessageModels
            {
                Data = this._unitOfMeasureService.Pagnition(pageSize, pageIndex, companyId, out statusCode),
                Status = new StatusCodeModels
                {
                    StatusCode = statusCode,
                    MessageId = CommonData.DisplayMessage(statusCode),
                    Version = "v1"
                }

            };

        }

        //Post: {"Code":"L","CompanyId":"2403221134200464037"}
        [Route("api/UnitOfMeasure/SearchByCode")]
        [HttpPost]
        public ResponseMessageModels SearchByCode(string codeName, string companyId)
        {
            int statusCode = 9;
            return new ResponseMessageModels
            {
                Data = this._unitOfMeasureService.SearchByCode(codeName, companyId, out statusCode),
                Status = new StatusCodeModels
                {
                    StatusCode = statusCode,
                    MessageId = CommonData.DisplayMessage(statusCode),
                    Version = "v1"
                }

            };

        }   
        private IUnitOfMeasureService _unitOfMeasureService;

    }
}
