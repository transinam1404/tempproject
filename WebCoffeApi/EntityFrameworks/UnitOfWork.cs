﻿
// Type: FasCon.Apis.EntityFrameworks.UnitOfWork




using FasCon.Apis.Patterns;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;


namespace FasCon.Apis.EntityFrameworks
{
  public class UnitOfWork : IUnitOfWork, IDisposable
  {
    private Database _context;
    private Dictionary<string, object> repositories;
        //private string sqlConnectionString = "data source=.\\SQLEXPRESS;Integrated Security=true;AttachDbFilename=" + Application.StartupPath + "\\Data\\LocalDB.mdf;User Instance=True;MultipleActiveResultSets=True";

        private string sqlConnectionString = "Data Source=SQL5079.site4now.net;Initial Catalog=db_9f5d36_kpi;User Id=db_9f5d36_kpi_admin;Password=sgu123!@#;MultipleActiveResultSets=true";
        private string providerName = "System.Data.SqlClient";
    private bool disposed;

    public UnitOfWork()
    {
      this._context = new Database(this.sqlConnectionString, this.providerName);
    }

    public FasCon.Apis.EntityFrameworks.GenericRepository<T> GenericRepository<T>() where T : class
    {
      if (this.repositories == null)
        this.repositories = new Dictionary<string, object>();
      string name = typeof (T).Name;
      if (!this.repositories.ContainsKey(name))
      {
        object instance = Activator.CreateInstance(typeof (FasCon.Apis.EntityFrameworks.GenericRepository<>).MakeGenericType(typeof (T)), (object) this._context);
        this.repositories.Add(name, instance);
      }
      return (FasCon.Apis.EntityFrameworks.GenericRepository<T>) this.repositories[name];
    }

    public void BeginTransaction(IsolationLevel isoLevel = IsolationLevel.ReadCommitted)
    {
      this._context.IsolationLevel = new IsolationLevel?(isoLevel);
      this._context.BeginTransaction();
    }

    public int CommitTransaction()
    {
      int num = 0;
      try
      {
        this._context.CompleteTransaction();
      }
      catch
      {
        num = 9;
      }
      return num;
    }

    public void RollBackTransaction() => this._context.AbortTransaction();

    public IEnumerable<TEntity> Query<TEntity>(string query, params object[] parameters)
    {
      return (IEnumerable<TEntity>) this._context.Fetch<TEntity>(query, parameters);
    }

    public object ExecuteScalar<TEntity>(string query, params object[] parameters)
    {
      return (object) this._context.ExecuteScalar<TEntity>(query, parameters);
    }

    public int Execute(string query, params object[] parameters)
    {
      return this._context.Execute(query, parameters);
    }

    public TEntity QueryFirstOrDefault<TEntity>(string query, params object[] parameters)
    {
      return this._context.FirstOrDefault<TEntity>(query, parameters);
    }

    public DataTable ExecuteQuery(string query)
    {
      DataTable dataTable = new DataTable();
      SqlConnection selectConnection = new SqlConnection(this._context.ConnectionString);
      try
      {
        selectConnection.Open();
        new SqlDataAdapter(query, selectConnection).Fill(dataTable);
      }
      finally
      {
        selectConnection?.Close();
      }
      return dataTable;
    }

    public bool CheckConnection()
    {
      SqlConnection sqlConnection = new SqlConnection(this._context.ConnectionString);
      try
      {
        sqlConnection.Open();
        return true;
      }
      finally
      {
        sqlConnection?.Close();
      }
    }

    public DataSet ExecuteQuery(string[] query, string[] tableName)
    {
      DataSet dataSet = new DataSet();
      SqlConnection selectConnection = new SqlConnection(this._context.ConnectionString);
      try
      {
        selectConnection.Open();
        for (int index = 0; index < query.Length; ++index)
        {
          DataTable dataTable = new DataTable();
          new SqlDataAdapter(query[index], selectConnection).Fill(dataTable);
          dataTable.TableName = tableName[index];
          dataSet.Tables.Add(dataTable);
        }
      }
      finally
      {
        selectConnection?.Close();
      }
      return dataSet;
    }

    protected virtual void Dispose(bool disposing)
    {
      if (!this.disposed && disposing)
        this._context.Dispose();
      this.disposed = true;
    }

    public void Dispose() => this._context.Dispose();
  }
}
