﻿
// Type: FasCon.Apis.Mapping.Auth_GroupUser




using PetaPoco;


namespace FasCon.Apis.Mapping
{
  [PrimaryKey("Id", AutoIncrement = false)]
  public class Auth_GroupUser
  {
    public long Id { get; set; }

    public string GroupName { get; set; }

    public string CompanyId { get; set; }

    public string Note { get; set; }

    public short AllowAppReport { get; set; }

    public short AllowAppOrder { get; set; }
  }
}
