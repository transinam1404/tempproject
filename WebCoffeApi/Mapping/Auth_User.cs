using System;
using PetaPoco;

namespace FasCon.Apis.Mapping
{
    [PrimaryKey("Id", AutoIncrement = false)]
    public class Auth_User
    {
        public long Id { get; set; }

        public string UserName { get; set; }

        public string Pwd { get; set; }

        public string CompanyId { get; set; }

        public string BranchId { get; set; }

        public short? isAdministrator { get; set; }

        public long? EmployeeId { get; set; }

        public string EmployeeName { get; set; }

        public long? GroupId { get; set; }

        public int? StatusId { get; set; }

        public short? IsUsed { get; set; }
    }
}
