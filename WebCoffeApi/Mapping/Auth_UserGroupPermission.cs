using System;
using PetaPoco;

namespace FasCon.Apis.Mapping
{
    [PrimaryKey("Id", AutoIncrement = false)]
    public class Auth_UserGroupPermission
    {
        public long Id { get; set; }

        public string GroupId { get; set; }

        public string ScreenCode { get; set; }

        public string ActionCode { get; set; }

        public string CompanyId { get; set; }

        public int StatusId { get; set; }

        public bool isEnable { get; set; }
    }
}
