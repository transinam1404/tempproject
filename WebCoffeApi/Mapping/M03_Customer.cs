using System;
using PetaPoco;

namespace FasCon.Apis.Mapping
{
	[PrimaryKey("Id", AutoIncrement = false)]
	public class M03_Customer
	{
		public long Id { get; set; }
		public string Code { get; set; }
		public string Name { get; set; }
		public string Search_Name { get; set; }
		public string Address { get; set; }
		public string PhoneNo { get; set; }
		public DateTime? Birthday { get; set; }
		public short? CountryId { get; set; }
		public short? CityId { get; set; }
		public string Email { get; set; }
		public string FaxNo { get; set; }
		public string Bank_Account { get; set; }
		public string TaxCode { get; set; }
		public string Legal_Representative { get; set; }
		public int? Posting_GroupId { get; set; }
		public short? CurrencyId { get; set; }
		public short? PaymentTermId { get; set; }
		public int? EmployeeId { get; set; }
		public short? VAT_Buss_GroupId { get; set; }
		public long? Cus_CategoryId { get; set; }
		public int? Price_GroupId { get; set; }
		public int? PriceId { get; set; }
		public decimal? Debit_Limit { get; set; }
		public short? IsUsed { get; set; }
		public string Note { get; set; }
		public string CompanyId { get; set; }
		public short? StatusId { get; set; }
		public short? IsDeleted { get; set; }
		public short SuDungTheVip { get; set; }
		public string MaTheVip { get; set; }
		public long LoaiTheVip { get; set; }
		public DateTime NgayPhatHanhThe { get; set; }
		public DateTime NgayHetHanThe { get; set; }
		public decimal MucChietKhau { get; set; }
		public decimal SoDiemMacDinh { get; set; }
		public decimal DiemTichLuy { get; set; }
		public string GhiChuTheVip { get; set; }
		public DateTime? CreatedDate { get; set; }
		public string CreatedBy { get; set; }
		public DateTime? UpdatedDate { get; set; }
		public string UpdatedBy { get; set; }
		public DateTime? ApprovedDate { get; set; }
		public string ApprovedBy { get; set; }
	}
}
