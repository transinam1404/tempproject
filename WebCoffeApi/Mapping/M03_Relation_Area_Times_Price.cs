using System;
using PetaPoco;

namespace FasCon.Apis.Mapping
{
    [PrimaryKey("Id", AutoIncrement = false)]
    public class M03_Relation_Area_Times_Price
    {
        public long Id { get; set; }
        public long? AreaId { get; set; }
        public long? TimeId { get; set; }
        public long? PriceId { get; set; }
        public string BranchId { get; set; }
        public short? IsDeleted { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
    }
}
