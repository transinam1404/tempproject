using System;
using PetaPoco;

namespace FasCon.Apis.Mapping
{
    [PrimaryKey("Id", AutoIncrement = false)]
    public class M03_Sale_Area
    {
        public long Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Printer { get; set; }
        public short? IsUsed { get; set; }
        public short? Arrangement { get; set; }
        public string Note { get; set; }
        public short? StatusId { get; set; }
        public string BranchId { get; set; }
        public string CompanyId { get; set; }
        public short? IsDeleted { get; set; }
        public short IsTinhGio { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? ApprovedDate { get; set; }
        public string ApprovedBy { get; set; }
        public decimal Discount { get; set; }
        public decimal DiscAmount { get; set; }
        public decimal Percent_ServiceCharge { get; set; }
        public decimal ServiceChargeAmount { get; set; }
        public decimal VAT_DiscAmount { get; set; }
    }
}
