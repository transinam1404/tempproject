using System;
using PetaPoco;

namespace FasCon.Apis.Mapping
{
	[PrimaryKey("Id", AutoIncrement = false)]
	public class M03_Sale_Area_Item
	{
		public long Id { get; set; }
		public long AreaId { get; set; }
		public long ItemId { get; set; }
		public decimal Qty { get; set; }
		public string BranchId { get; set; }
		public short IsDeleted { get; set; }
	}
}
