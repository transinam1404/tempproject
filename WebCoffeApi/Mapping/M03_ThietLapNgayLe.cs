using System;
using PetaPoco;

namespace FasCon.Apis.Mapping
{
	[PrimaryKey("Id", AutoIncrement = false)]
	public class M03_ThietLapNgayLe
	{
		public long Id { get; set; }
		public DateTime NgayLe { get; set; }
		public string GhiChu { get; set; }
		public string CompanyId { get; set; }
		public short IsDeleted { get; set; }
		public DateTime CreatedDate { get; set; }
		public string CreatedBy { get; set; }
		public DateTime UpdatedDate { get; set; }
		public string UpdatedBy { get; set; }
	}
}
