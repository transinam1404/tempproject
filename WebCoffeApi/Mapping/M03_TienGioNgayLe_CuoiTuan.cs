using System;
using PetaPoco;

namespace DanTriSoft.Apis.Mapping
{
    [PrimaryKey("Id", AutoIncrement = false)]
    public class M03_TienGioNgayLe_CuoiTuan
    {
        public long Id { get; set; }
        public long AreaId { get; set; }
        public long TimeId { get; set; }
        public decimal PriceLastWeek { get; set; }
        public decimal PriceHoliday { get; set; }
        public short IsUsed { get; set; }
        public short ThuBay { get; set; }
        public short ChuNhat { get; set; }
        public short NgayLe { get; set; }
        public string CompanyId { get; set; }
        public string BranchId { get; set; }
    }
}
