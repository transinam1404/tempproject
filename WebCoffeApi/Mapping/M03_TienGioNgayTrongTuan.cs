using System;
using PetaPoco;

namespace FasCon.Apis.Mapping
{
    [PrimaryKey("Id", AutoIncrement = false)]
    public class M03_TienGioNgayTrongTuan
    {
        public long Id { get; set; }
        public long AreaId { get; set; }
        public long TimeId { get; set; }
        public decimal Price { get; set; }
        public short IsUsed { get; set; }
        public string CompanyId { get; set; }
        public string BranchId { get; set; }
    }
}
