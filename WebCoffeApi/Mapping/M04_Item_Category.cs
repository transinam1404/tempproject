using System;
using PetaPoco;

namespace FasCon.Apis.Mapping
{
    [PrimaryKey("Id", AutoIncrement = false)]
    public class M04_Item_Category
    {
        public long Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Printer { get; set; }
        public short? StatusId { get; set; }
        public string Note { get; set; }
        public string CompanyId { get; set; }
        public short? IsDeleted { get; set; }
        public short AllowPrinter { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? ApprovedDate { get; set; }
        public string ApprovedBy { get; set; }
    }
}
