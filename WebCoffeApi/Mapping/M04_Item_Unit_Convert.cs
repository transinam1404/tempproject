using System;
using PetaPoco;

namespace FasCon.Apis.Mapping
{
    [PrimaryKey("Id", AutoIncrement = false)]
    public class M04_Item_Unit_Convert
    {
        public long Id { get; set; }
        public long ItemId { get; set; }
        public long UnitId { get; set; }
        public decimal HeSoQuyDoi { get; set; }
        public string Barcode { get; set; }
        public string Note { get; set; }
        public string CompanyId { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
    }
}
