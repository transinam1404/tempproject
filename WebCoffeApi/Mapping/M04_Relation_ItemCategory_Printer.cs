using System;
using PetaPoco;

namespace FasCon.Apis.Mapping
{
    [PrimaryKey("Id", AutoIncrement = false)]
    public class M04_Relation_ItemCategory_Printer
    {
        public long Id { get; set; }
        public long Item_CategoryId { get; set; }
        public string Printer { get; set; }
        public string BranchId { get; set; }
        public string CompanyId { get; set; }
    }
}
