﻿
// Type: FasCon.Apis.Mapping.M05_Employee




using PetaPoco;
using System;


namespace FasCon.Apis.Mapping
{
  [PrimaryKey("Id", AutoIncrement = false)]
  public class M05_Employee
  {
    public long Id { get; set; }

    public string Code { get; set; }

    public string Name { get; set; }

    public string Search_Name { get; set; }

    public DateTime? BirthDay { get; set; }

    public string PlaceOfBirth { get; set; }

    public string Address { get; set; }

    public string PhoneNo { get; set; }

    public short? CountryId { get; set; }

    public string IDNumber { get; set; }

    public string Email { get; set; }

    public string Ethnic { get; set; }

    public short? Sex { get; set; }

    public string TaxIdNo { get; set; }

    public int? DeptId { get; set; }

    public int? ShiftId { get; set; }

    public short? IsUsed { get; set; }

    public string Note { get; set; }

    public short? StatusId { get; set; }

    public string BranchId { get; set; }

    public string CompanyId { get; set; }

    public short? IsDeleted { get; set; }

    public DateTime? CreatedDate { get; set; }

    public string CreatedBy { get; set; }

    public DateTime? UpdatedDate { get; set; }

    public string UpdatedBy { get; set; }

    public DateTime? ApprovedDate { get; set; }

    public string ApprovedBy { get; set; }
  }
}
