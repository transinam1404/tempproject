﻿
// Type: FasCon.Apis.Mapping.M05_Shift




using PetaPoco;
using System;


namespace FasCon.Apis.Mapping
{
  [PrimaryKey("Id")]
  public class M05_Shift
  {
    public long Id { get; set; }

    public string Code { get; set; }

    public string Name { get; set; }

    public string Note { get; set; }

    public short? StatusId { get; set; }

    public string CompanyId { get; set; }

    public short? IsDeleted { get; set; }

    public DateTime? CreatedDate { get; set; }

    public string CreatedBy { get; set; }

    public DateTime? UpdatedDate { get; set; }

    public string UpdatedBy { get; set; }

    public DateTime? ApprovedDate { get; set; }

    public string ApprovedBy { get; set; }
  }
}
