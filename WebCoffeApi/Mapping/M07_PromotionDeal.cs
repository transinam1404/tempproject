﻿
// Type: FasCon.Apis.Mapping.M07_PromotionDeal




using PetaPoco;
using System;


namespace FasCon.Apis.Mapping
{
  [PrimaryKey("Id", AutoIncrement = false)]
  public class M07_PromotionDeal
  {
    public string Id { get; set; }

    public string DocumentNo { get; set; }

    public string PromotionName { get; set; }

    public DateTime? FromDate { get; set; }

    public DateTime? ToDate { get; set; }

    public short? IsUsed { get; set; }

    public long? EmployeeId { get; set; }

    public short? PromotionType { get; set; }

    public string Note { get; set; }

    public string CompanyId { get; set; }

    public DateTime? CreatedDate { get; set; }

    public string CreatedBy { get; set; }

    public DateTime? UpdatedDate { get; set; }

    public string UpdatedBy { get; set; }

    public short? IsDeleted { get; set; }

    public int FromHour { get; set; }

    public int FromMinute { get; set; }

    public int ToHour { get; set; }

    public int ToMinute { get; set; }

    public int ApDungGioGoiMon_VaoBan { get; set; }
  }
}
