﻿
// Type: FasCon.Apis.Mapping.M07_Promotion_Item




using PetaPoco;
using System;


namespace FasCon.Apis.Mapping
{
  [PrimaryKey("Id", AutoIncrement = false)]
  public class M07_Promotion_Item
  {
    public long Id { get; set; }

    public string DocInfoId { get; set; }

    public string DocumentNo { get; set; }

    public int? PromotionLevel { get; set; }

    public long? ItemId { get; set; }

    public int? UomId { get; set; }

    public Decimal? Quantity { get; set; }

    public Decimal? Discount { get; set; }

    public Decimal? DiscAmount { get; set; }

    public Decimal? DiscAmountEdit { get; set; }

    public string Note { get; set; }

    public string CompanyId { get; set; }

    public short? Canceled { get; set; }

    public short? IsDeleted { get; set; }
  }
}
