﻿
// Type: FasCon.Apis.Mapping.S00_Branch




using PetaPoco;
using System;


namespace FasCon.Apis.Mapping
{
  [PrimaryKey("Id", AutoIncrement = false)]
  public class S00_Branch
  {
    public string Id { get; set; }

    public string Code { get; set; }

    public string Name { get; set; }

    public string Search_Name { get; set; }

    public string Address { get; set; }

    public short? CountryId { get; set; }

    public short? CityId { get; set; }

    public short? RegionId { get; set; }

    public string PhoneNo { get; set; }

    public string FaxNo { get; set; }

    public string TaxCode { get; set; }

    public string Website { get; set; }

    public string Email { get; set; }

    public short? MCP { get; set; }

    public string Latitude { get; set; }

    public string Longitude { get; set; }

    public DateTime? PostingFrom { get; set; }

    public DateTime? PostingTo { get; set; }

    public string CompanyId { get; set; }

    public string FormatAmount { get; set; }

    public string FormatQty { get; set; }

    public short? IsUsed { get; set; }

    public short? StatusId { get; set; }

    public string Note { get; set; }

    public short? HinhThucTraSua { get; set; }

    public string MaNganh { get; set; }

    public string LoaiHinhKinhDoanh { get; set; }

    public string PartnerCode { get; set; }

    public DateTime? CreatedDate { get; set; }

    public string CreatedBy { get; set; }

    public DateTime? UpdatedDate { get; set; }

    public string UpdatedBy { get; set; }

    public DateTime? ApprovedDate { get; set; }

    public string ApprovedBy { get; set; }
  }
}
