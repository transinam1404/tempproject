﻿
// Type: FasCon.Apis.Mapping.T03_Sales_Invoice_HinhThucTT




using PetaPoco;
using System;


namespace FasCon.Apis.Mapping
{
  [PrimaryKey("Id")]
  public class T03_Sales_Invoice_HinhThucTT
  {
    public long Id { get; set; }

    public string Info_Id { get; set; }

    public long Bank_AccountId { get; set; }

    public Decimal Amount { get; set; }

    public string SoTaiKhoan { get; set; }

    public string TaiKhoanNhan { get; set; }

    public string BranchId { get; set; }
  }
}
