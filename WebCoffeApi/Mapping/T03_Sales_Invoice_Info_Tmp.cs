﻿
// Type: FasCon.Apis.Mapping.T03_Sales_Invoice_Info_Tmp




using PetaPoco;
using System;


namespace FasCon.Apis.Mapping
{
  [PrimaryKey("Id", AutoIncrement = false)]
  public class T03_Sales_Invoice_Info_Tmp
  {
    public string Id { get; set; }

    public string DocumentNo { get; set; }

    public string External_DocumentNo { get; set; }

    public DateTime? PostingDate { get; set; }

    public DateTime? DocumentDate { get; set; }

    public long? CustomerId { get; set; }

    public string Note { get; set; }

    public string Address { get; set; }

    public string PhoneNo { get; set; }

    public string Description_Delivery { get; set; }

    public string DeliveryName { get; set; }

    public long? SellerId { get; set; }

    public DateTime? CheckIn_Date { get; set; }

    public DateTime? Printer_Date { get; set; }

    public long? AreaId { get; set; }

    public long? TableId { get; set; }

    public long? EmployeeOrderId { get; set; }

    public long? EmployeeServiceId { get; set; }

    public long? EmployeeCreatedId { get; set; }

    public short? PaymentTermId { get; set; }

    public DateTime? ExpectRreceipt_Date { get; set; }

    public DateTime? DueDate { get; set; }

    public long? LocationId { get; set; }

    public string CompanyId { get; set; }

    public string BranchId { get; set; }

    public long? To_LocationId { get; set; }

    public string To_BranchId { get; set; }

    public short? CurrencyId { get; set; }

    public Decimal? ExchangeRate { get; set; }

    public short? DocumentType { get; set; }

    public short? Invoice { get; set; }

    public Decimal? InAdvanced { get; set; }

    public Decimal? Remain { get; set; }

    public Decimal? Info_Amount { get; set; }

    public Decimal? Info_Amount_LCY { get; set; }

    public Decimal? Info_Discount { get; set; }

    public Decimal? Info_DiscAmount { get; set; }

    public Decimal? Info_Percent_ServiceCharge { get; set; }

    public Decimal? Info_ServiceChargeAmount { get; set; }

    public Decimal? Info_VAT_DiscAmount { get; set; }

    public Decimal? Info_VatAmount { get; set; }

    public Decimal? Info_VAT_Amount_LCY { get; set; }

    public Decimal? Info_TotalAmount { get; set; }

    public Decimal? Info_TotalAmount_LCY { get; set; }

    public short Info_Dis_AmountEdit { get; set; }

    public short Info_ServiceChargeAmtEdit { get; set; }

    public short? Is_Direct_Receipt { get; set; }

    public string Description { get; set; }

    public short? StatusId { get; set; }

    public string DocumentNo_Ref { get; set; }

    public long VipId { get; set; }

    public short OrdinalNumber { get; set; }

    public DateTime? CreatedDate { get; set; }

    public string CreatedBy { get; set; }

    public DateTime? UpdatedDate { get; set; }

    public string UpdatedBy { get; set; }

    public DateTime? ApprovedDate { get; set; }

    public string ApprovedBy { get; set; }

    public int CurrentVersion { get; set; }

    public long ShipperId { get; set; }

    public string IPAddress { get; set; }

    public string MachineName { get; set; }

    public string TenKH_TraSua { get; set; }

    public short IsTinhGio { get; set; }

    public Decimal TongSoGio { get; set; }

    public Decimal TongTienGio { get; set; }

    public short IsSaveOffline { get; set; }

    public short SoLanInHoaDon { get; set; }

    public string TenKH { get; set; }

    public string DiaChiKH { get; set; }

    public string DienThoaiKH { get; set; }

    public Decimal SoTienKhachDua { get; set; }

    public Decimal TienTraLaiKhach { get; set; }

    public int SoLuongKH { get; set; }

    public Decimal TTTienMat { get; set; }

    public Decimal TTNganHang { get; set; }

    public long NganHangId { get; set; }

    public Decimal TTVoucher { get; set; }

    public long VoucherId { get; set; }
  }
}
