using System;

namespace FasCon.Apis.Models
{
    public class BaseModels
    {
        public string CompanyId { get; set; }
        public string BranchId { get; set; }
        public long UserID { get; set; }
        public string UserName { get; set; }
        public long AreaId { get; set; }
        public long TableId { get; set; }
        public long ItemId { get; set; }
        public string Size_TraSua { get; set; }
        public string LoginID { get; set; }
        public object Value01 { get; set; }
        public object Value02 { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string MasterType { get; set; }
        public long EmployeeId { get; set; }
        public object ListObject { get; set; }
        public string IPAddress { get; set; }
        public string MachineName { get; set; }
        public string LoaiPM { get; set; }
        public string LoaiHinhKinhDoanh { get; set; }
        public DateTime Ngay { get; set; }
        public string InfoId { get; set; }
        public decimal TienKhachDua { get; set; }
        public decimal TienTraLai { get; set; }
        public decimal TTTienMat { get; set; }
        public decimal TTNganHang { get; set; }
        public long NganHangId { get; set; }
        public decimal TTVoucher { get; set; }
        public long VoucherId { get; set; }

        public BaseModels()
        {
            Size_TraSua = string.Empty;
            MasterType = string.Empty;
            Value02 = string.Empty;
            Ngay = DateTime.Now;
        }
    }
}
