﻿
// Type: FasCon.Apis.Models.RoleModels





namespace FasCon.Apis.Models
{
  public class RoleModels
  {
    public string UserName { get; set; }

    public string CompanyId { get; set; }

    public string BranchId { get; set; }

    public string ScreenCode { get; set; }

    public string ActionCode { get; set; }

    public bool IsEnable { get; set; }

    public short IsAdministrator { get; set; }
  }
}
