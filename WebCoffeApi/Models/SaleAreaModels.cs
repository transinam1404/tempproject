﻿// Type: FasCon.Apis.Models.SaleAreaModels

namespace WebCoffeApi.Models
{
    public class SaleAreaModels
    {
        public long ID { get; set; }

        public string Code { get; set; }

        public string Name { get; set; }

        public string Printer { get; set; }

        public short Arrangement { get; set; }

        public short IsUsed { get; set; }

        public string Note { get; set; }

        public string BranchId { get; set; }

        public string CompanyId { get; set; }

        public string UserName { get; set; }

        public short IsDeleted { get; set; }

        public short IsTinhGio { get; set; }

        public SaleAreaModels()
        {
            Code = string.Empty;
            Name = string.Empty;
            Printer = string.Empty;
            Arrangement = 0;
            IsUsed = 1;
            Note = string.Empty;
            BranchId = string.Empty;
            CompanyId = string.Empty;
            IsDeleted = 0;
            IsTinhGio = 0;
        }
    }
}
