﻿
// Type: FasCon.Apis.Models.TokenModels




using Newtonsoft.Json;


namespace FasCon.Apis.Models
{
  public class TokenModels
  {
    [JsonProperty("access_token")]
    public string accessToken { get; set; }

    public string userName { get; set; }

    public string subDomain { get; set; }

    public string MaGianHang { get; set; }

    public string CompanyId { get; set; }

    public string BranchId { get; set; }

    public string BranchName { get; set; }

    public string Address { get; set; }

    public string AddOn { get; set; }

    [JsonProperty("token_type")]
    public string TokenType { get; set; }

    [JsonProperty("expires_in")]
    public int ExpiresIn { get; set; }

    [JsonProperty("refresh_token")]
    public string RefreshToken { get; set; }

    public string IsAdministrator { get; set; }

    public string BanHangCanTeen { get; set; }

    public string InVaLuuHoaDon { get; set; }

    public long userId { get; set; }

    public string MaNganh { get; set; }

    public string LoaiHinhKinhDoanh { get; set; }

    public string TraSua_SuaSoLuong { get; set; }

    public string EmployeeName { get; set; }

    public int LamTronTien_BanHang { get; set; }

    public int NumDeviceConnect { get; set; }
  }
}
