﻿
// Type: FasCon.Apis.Models.UserLoginModels




using System.Collections.Generic;


namespace FasCon.Apis.Models
{
  public class UserLoginModels
  {
    public string BranchId { get; set; }

    public string BranchCode { get; set; }

    public string CompanyId { get; set; }

    public string CompanyCode { get; set; }

    public long UserId { get; set; }

    public string UserName { get; set; }

    public string Password { get; set; }

    public short IsAdministrator { get; set; }

    public int EmployeeId { get; set; }

    public List<ComboboxSelectModels> listDefaultValue { get; set; }

    public string BranchName { get; set; }

    public string Address { get; set; }

    public bool HinhThucTraSua { get; set; }

    public string LoginDevice { get; set; }

    public short AllowAppReport { get; set; }

    public short AllowAppOrder { get; set; }

    public short BanHangCanTeen { get; set; }

    public short InVaLuuHoaDon { get; set; }

    public string MaNganh { get; set; }

    public string LoaiHinhKinhDoanh { get; set; }

    public short TraSua_SuaSoLuong { get; set; }

    public string PartnerName { get; set; }

    public string Phone { get; set; }

    public string Email { get; set; }

    public UserLoginModels()
    {
      this.CompanyId = string.Empty;
      this.CompanyCode = string.Empty;
      this.BranchId = string.Empty;
      this.BranchCode = string.Empty;
      this.MaNganh = string.Empty;
      this.LoaiHinhKinhDoanh = string.Empty;
    }
  }
}
