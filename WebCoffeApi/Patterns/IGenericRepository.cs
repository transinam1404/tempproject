using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace FasCon.Apis.Patterns
{
	public interface IGenericRepository<TEntity> where TEntity : class
	{
		IEnumerable<TEntity> Query(string CompanyId, string BranchId);

		IEnumerable<TEntity> Query(string CompanyId, string BranchId, Expression<Func<TEntity, bool>> predicate);

		IEnumerable<TEntity> Query(string sql, params object[] args);

		TEntity FirstOrDefault(string sql, params object[] args);

		TEntity FirstOrDefault(string CompanyId, string BranchId, Expression<Func<TEntity, bool>> predicate);

		TEntity FirstOrDefault(string CompanyId, string BranchId, object id);

		int Execute(string sql, params object[] args);

		void Insert(TEntity entity);

		void Update(TEntity entity);

		void Delete(TEntity entity);
	}
}
