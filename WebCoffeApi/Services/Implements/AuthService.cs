 
using System;
using System.Collections.Generic;
using System.Linq;
using FasCon.Apis.Common;
using FasCon.Apis.EntityFrameworks;
using FasCon.Apis.Models;

namespace FasCon.Apis.Services
{
    //Nam fixed this service.
    public class AuthService : IAuthService
	{
		public AuthService()
		{
			this._unitOfWork = new UnitOfWork();
		}

		public UserLoginModels CheckLogin(UserLoginModels model, out int statusCode)
		{
			statusCode = 9;
            UserLoginModels userLoginModels = this._unitOfWork.QueryFirstOrDefault<UserLoginModels>(";exec S005_CheckLogin_Offline @BranchCode,@UserName,@Pass", new object[]
			{
				new
				{
					BranchCode = model.BranchCode,
					UserName = model.UserName,
					//Pass = CommonFunction.EncrypteMD5(model.Password)
					//Pass=CommonFunction.EncryptSHA(model.Password)
					Pass=model.Password // for back-end test only
                }
			});
			if (userLoginModels != null)
			{
				statusCode = 0;
                userLoginModels.listDefaultValue = this._unitOfWork.Query<ComboboxSelectModels>("select Id,Code,Name,Field3 = Value from sy_Params where BranchId = @BranchId ", new object[]
				{
					new
					{
                        userLoginModels.BranchId
					}
				}).ToList<ComboboxSelectModels>();
			}
			return userLoginModels;
		}

		public IEnumerable<RoleModels> GetListRoleByUser(RoleModels model)
		{
			return this._unitOfWork.Query<RoleModels>("\r\n            select  \r\n                    b.UserName,\r\n                    a.ActionCode,\r\n                    a.ScreenCode,\r\n                    a.IsEnable,\r\n                    a.CompanyId\r\n            from Auth_UserGroupPermission a\r\n            join Auth_User b on a.CompanyId = b.CompanyId\r\n            join Auth_GroupUser c on a.GroupId = c.Id and b.GroupId = c.Id\r\n            where a.CompanyId = @CompanyId\r\n            and b.UserName = @UserName\r\n            and a.isEnable = 1\r\n\r\n            union all \r\n\r\n            select  \r\n                    b.UserName,\r\n                    a.ActionCode,\r\n                    a.ScreenCode,\r\n                    a.IsEnable,\r\n                    a.CompanyId\r\n            from Auth_UserGroupPermission a\r\n            join Auth_User b on a.CompanyId = b.CompanyId\r\n            join Auth_GroupUser c on a.GroupId = c.Id and b.GroupId = c.Id\r\n            where a.CompanyId = @CompanyId\r\n            and b.UserName = @UserName\r\n            and a.ScreenCode = 'Restaurant'\r\n            and a.isEnable = 0\r\n\r\n            ", new object[]
			{
				new
				{
					model.CompanyId,
					model.UserName
				}
			}).ToArray<RoleModels>();
		}


		private UnitOfWork _unitOfWork;
	}
}