﻿using FasCon.Apis.EntityFrameworks;
using System.Runtime.CompilerServices;
using FasCon.Apis.Models;
using FasCon.Apis.Mapping;
using System.Data;
using System.Linq;
namespace FasCon.Apis.Services
{
    public class CustomerCategoryService : ICustomerCategoryService
    {
        private UnitOfWork _unitOfWork;
        public CustomerCategoryService() {
            this._unitOfWork = new UnitOfWork();
        }
        public IEnumerable<CustomerCategoryModels> GetAll(CustomerCategoryModels searchModel, out int statusCode)
        {
            statusCode = 9;
            IEnumerable<CustomerCategoryModels> result = _unitOfWork.Query<CustomerCategoryModels>(
                "SELECT * FROM M03_Customer_Category WHERE CompanyId = @Id",
                new { Id = searchModel.CompanyId }
            ).ToList();

            if (result != null && result.Any())
            {
                statusCode = 0;
                return result;
            }

            return result;
        }

        public void Add(CustomerCategoryModels model, out int statusCode)
        {
            var entity = new M03_Customer_Category
            {
                Id = this.GetLastCustomerId() + 1,
                Code = model.Code,
                Name = model.Name,
                Note = model.Note,
                //StatusId = model.StatusId,
                CompanyId = model.CompanyId,//
                IsDeleted = 0,//
                CreatedDate = System.DateTime.Now,//
                CreatedBy = model.CreatedBy,//
                UpdatedDate = DateTime.Now,
                UpdatedBy = model.UpdatedBy,
                //ApprovedDate = model.ApprovedDate,
                //ApprovedBy = model.ApprovedBy
            };
            try
            {
                this._unitOfWork.BeginTransaction(IsolationLevel.ReadUncommitted);
                this._unitOfWork.GenericRepository<M03_Customer_Category>().Insert(entity);

                statusCode = this._unitOfWork.CommitTransaction();
            }
            catch (Exception ex)
            {
                statusCode = 9;
                this._unitOfWork.RollBackTransaction();
                throw ex;
            }

        }
        public int GetLastCustomerId()
        {
            return this._unitOfWork.Query<int>("select max(Id) from M03_Customer_Category", new object[]
            {

            }).FirstOrDefault<int>();
        }

        public void Delete(long id, string updateBy, string companyId, out int statusCode)
        {
            try
            {
                var entity = this.GetCustomerCategoryById(id, companyId);
                if (entity != null)
                {
                    entity.IsDeleted = 1;
                    entity.UpdatedBy = updateBy;
                    entity.UpdatedDate = DateTime.Now;

                    this._unitOfWork.BeginTransaction(IsolationLevel.ReadUncommitted);
                    this._unitOfWork.GenericRepository<M03_Customer_Category>().Update(entity);
                    statusCode = this._unitOfWork.CommitTransaction();

                    if (isUnitOfCustomerCategoryExistInItem(id))
                    {
                        var relatedItems = this._unitOfWork.Query<M03_Customer>("SELECT * FROM M03_Customer WHERE Cus_CategoryId = @Id", new { Id = id });
                        foreach (var item in relatedItems)
                        {
                            item.IsDeleted = 1;
                            item.UpdatedBy = updateBy;
                            item.UpdatedDate = DateTime.Now;
                            this._unitOfWork.GenericRepository<M03_Customer>().Update(item);
                        }

                        statusCode = this._unitOfWork.CommitTransaction();
                    }
                }
                else
                {
                    statusCode = 9;
                }
            }
            catch (Exception ex)
            {
                this._unitOfWork.RollBackTransaction();
                statusCode = 9;
                throw ex;
            }
        }



        public M03_Customer_Category GetCustomerCategoryById(long Id,string companyId)
        {
            var customer= this._unitOfWork.Query<M03_Customer_Category>("select * from M03_Customer_Category where Id = @Id and CompanyId=@CompanyId", new object[]
           {
               new
               {
                   Id=Id,
                   CompanyId=companyId

               }
           }).FirstOrDefault<M03_Customer_Category>();

            return customer;
        }

        public bool isUnitOfCustomerCategoryExistInItem(long id)
        {
           var result = this._unitOfWork.Query<M03_Customer>("select * from M03_Customer where Cus_CategoryId = @Id", new object[]
           {
               new
               {
                   Id=id
               }
           }).FirstOrDefault<M03_Customer>();
            if (result != null)
            {
                return true;
            }
            return false;   
        }


        public M03_Customer_Category GetCustomerCategoryById(long id, out int statusCode)
        {
            try
            {
                var result = this._unitOfWork.Query<M03_Customer_Category>("select * from M03_Customer_Category where Id = @Id", new object[]{
               new
               {
                   Id=id
               }}).FirstOrDefault<M03_Customer_Category>();
                if (result != null)
                {
                    statusCode = 0;
                    return result;
                }
                else
                {
                    statusCode = 9;
                    return null;
                }
            }
            catch
            {
                statusCode = 9;
                return null;
            }
        }

        public void Update(CustomerCategoryModels model, out int statusCode)
        {
            try
            {
                var entity = this.  GetCustomerCategoryById(model.Id, model.CompanyId);
                if (entity != null)
                {
                    entity.Code = model.Code;
                    entity.Name = model.Name;
                    entity.Note = model.Note;
                    entity.UpdatedBy = model.UpdatedBy;
                    entity.UpdatedDate = DateTime.Now;
                    this._unitOfWork.BeginTransaction(IsolationLevel.ReadUncommitted);
                    this._unitOfWork.GenericRepository<M03_Customer_Category>().Update(entity);
                    statusCode = this._unitOfWork.CommitTransaction();
                }
                else
                {
                    statusCode = 9;
                }

            }
            catch
            {
                this._unitOfWork.RollBackTransaction();
                statusCode = 9;
            }
        }


        public IEnumerable<CustomerCategoryModels> Pagnition(int pageSize, int pageIndex, string companyId, out int statusCode)
        {
            try
            {
                var result = this._unitOfWork.Query<CustomerCategoryModels>("select * from M03_Customer_Category where CompanyId=@CompanyId ", new object[]
                {
                    new
                    {
                        CompanyId=companyId
                    }
                }).Skip(pageSize * (pageIndex - 1)).Take(pageSize).ToArray<CustomerCategoryModels>();
                if (result != null)
                {
                    statusCode = 0;
                    return result;
                }
                else
                {
                    statusCode = 9;
                    return null;
                }

            }
            catch
            {

                statusCode = 9;
                return null;

            }
        }


        public IEnumerable<M03_Customer_Category> SearchByCode(string codeName, string companyId, out int statusCode)
        {
            statusCode = 0;
            try
            {
                var result = this._unitOfWork.Query<M03_Customer_Category>("select * from M03_Customer_Category WHERE CompanyId = @CompanyId AND Code LIKE @CodeName", new object[]
                {
                    new
                    {
                        CompanyId=companyId,
                        CodeName="%" + codeName + "%"
                    }
                }).ToArray<M03_Customer_Category>();
                return result;
            }
            catch
            {

                statusCode = 9;
                return null;
            }
        }
    }



}

