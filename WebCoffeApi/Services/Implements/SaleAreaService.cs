﻿
// Type: FasCon.Apis.Services.SaleAreaService




using FasCon.Apis.EntityFrameworks;
using FasCon.Apis.Mapping;
using FasCon.Apis.Models;
using FasCon.Apis.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Web.Http;
using WebCoffeApi.Models;


namespace WebCoffeApi.Services.Implements
{
    public class SaleAreaService : ISaleAreaService
    {
        private UnitOfWork _unitOfWork;

        public SaleAreaService() => _unitOfWork = new UnitOfWork();

        public IEnumerable<SaleAreaModels> GetList(SaleAreaModels modelSearch , out int statusCode    )
        {
            statusCode = 9;
            IEnumerable<SaleAreaModels> result = _unitOfWork.GenericRepository<M03_Sale_Area>().Query(modelSearch.CompanyId, modelSearch.BranchId, t => t.IsUsed == 1 && t.IsDeleted == 0).Select(t => new SaleAreaModels()
            {
                ID = t.Id,
                Code = t.Code,
                Name = t.Name,
                Printer = t.Printer,
                IsUsed = t.IsUsed.HasValue ? t.IsUsed.Value : (short)1,
                BranchId = t.BranchId,
                CompanyId = t.CompanyId,
                Note = t.Note
            }).ToArray();
            if (result != null)
            {
                statusCode = 0;
                return result;
            }
            return result;

            //return _unitOfWork.GenericRepository<M03_Sale_Area>().Query(modelSearch.CompanyId, modelSearch.BranchId, t => t.IsUsed == 1 && t.IsDeleted == 0).Select(t => new SaleAreaModels()
            //{
            //    ID = t.Id,
            //    Code = t.Code,
            //    Name = t.Name,
            //    Printer = t.Printer,
            //    IsUsed = t.IsUsed.HasValue ? t.IsUsed.Value : (short)1,
            //    BranchId = t.BranchId,
            //    CompanyId = t.CompanyId,
            //    Note = t.Note
            //}).ToList();
        }

        public SaleAreaModels SelectByID(SaleAreaModels model)
        {
            M03_Sale_Area m03SaleArea = _unitOfWork.GenericRepository<M03_Sale_Area>().FirstOrDefault(model.CompanyId, model.BranchId, model.ID);
            if (m03SaleArea == null)
                return new SaleAreaModels();
            SaleAreaModels saleAreaModels = new SaleAreaModels();
            saleAreaModels.ID = m03SaleArea.Id;
            saleAreaModels.Code = m03SaleArea.Code;
            saleAreaModels.Name = m03SaleArea.Name;
            saleAreaModels.Printer = m03SaleArea.Printer;
            saleAreaModels.Arrangement = m03SaleArea.Arrangement.Value;
            short? nullable = m03SaleArea.IsUsed;
            int num1;
            if (!nullable.HasValue)
            {
                num1 = 1;
            }
            else
            {
                nullable = m03SaleArea.IsUsed;
                num1 = nullable.Value;
            }
            saleAreaModels.IsUsed = (short)num1;
            saleAreaModels.BranchId = m03SaleArea.BranchId;
            saleAreaModels.CompanyId = m03SaleArea.CompanyId;
            saleAreaModels.Note = m03SaleArea.Note;
            nullable = m03SaleArea.IsDeleted;
            int num2;
            if (!nullable.HasValue)
            {
                num2 = 0;
            }
            else
            {
                nullable = m03SaleArea.IsDeleted;
                num2 = nullable.Value;
            }
            saleAreaModels.IsDeleted = (short)num2;
            return saleAreaModels;
        }

        public bool IsCodeExists(string BranchID, string Code)
        {
            bool flag = false;
            if (_unitOfWork.GenericRepository<M03_Sale_Area>().FirstOrDefault(null, BranchID, t => t.Code.ToLower().Equals(Code.Trim().ToLower()) && t.IsDeleted == 0) != null)
                flag = true;
            return flag;
        }

        public SaleAreaModels Insert(SaleAreaModels model)
        {
            M03_Sale_Area entity = new M03_Sale_Area()
            {
                Id = this.GetLastSaleAreaId() + new Random().Next(1, 21),
                Code = model.Code.Trim().ToUpper(),
                Name = model.Name,
                Printer = model.Printer,
                Arrangement = new short?(model.Arrangement),
                IsUsed = new short?(model.IsUsed),
                BranchId = model.BranchId,
                CompanyId = model.CompanyId,
                Note = model.Note,
                IsDeleted = new short?(0),
                CreatedDate = new DateTime?(DateTime.Now),
                CreatedBy = model.UserName,
                UpdatedBy = model.UserName,
                UpdatedDate = new DateTime?(DateTime.Now)
            };
            _unitOfWork.GenericRepository<M03_Sale_Area>().Insert(entity);
            return model;
        }

        public SaleAreaModels Update(SaleAreaModels model)
        {
            M03_Sale_Area entity = _unitOfWork.GenericRepository<M03_Sale_Area>().FirstOrDefault(model.CompanyId, model.BranchId, model.ID);
            entity.Code = model.Code.ToUpper();
            entity.Name = model.Name;
            entity.Printer = model.Printer;
            entity.Arrangement = new short?(model.Arrangement);
            entity.IsUsed = new short?(model.IsUsed);
            entity.Note = model.Note;
            entity.IsDeleted = new short?(0);
            entity.UpdatedDate = new DateTime?(DateTime.Now);
            entity.UpdatedBy = model.UserName;
            _unitOfWork.GenericRepository<M03_Sale_Area>().Update(entity);
            return model;
        }

        public int KiemTraTonTaiID(string CompanyId, string BranchId, long id)
        {
            return _unitOfWork.GenericRepository<M03_Sale_Area>().FirstOrDefault(CompanyId, BranchId, id) != null ? 1 : 0;
        }

        // delete one item with status code 
       
        public M03_Sale_Area Delete(string CompanyId, string BranchId, string UserName, long Id, out int statusCode)
        {
            try
            {
                _unitOfWork.BeginTransaction(IsolationLevel.ReadCommitted);
                var entityExists = this.KiemTraTonTaiID(CompanyId, BranchId, Id) == 1;
                if (!entityExists)
                {
                    statusCode = 9;
                    _unitOfWork.RollBackTransaction();
                    return null;
                }

                M03_Sale_Area entity = _unitOfWork.GenericRepository<M03_Sale_Area>().FirstOrDefault(CompanyId, BranchId, Id);
                if (entity != null)
                {
                    if (entity.IsUsed == 1)
                    {
                        entity.IsDeleted = 1;
                        entity.UpdatedBy = UserName;
                        entity.UpdatedDate = DateTime.Now;
                        _unitOfWork.GenericRepository<M03_Sale_Area>().Update(entity);
                    }
                    else
                    {
                        // If the entity is not in use, delete it from the database
                        _unitOfWork.GenericRepository<M03_Sale_Area>().Delete(entity);
                    }

                    statusCode = _unitOfWork.CommitTransaction();
                }
                else
                {
                    statusCode = 9;
                    _unitOfWork.RollBackTransaction();
                }

                return entity;



            }
            catch (Exception ex)
            {
                _unitOfWork.RollBackTransaction();
                throw ex;
            }
        }

        public int Delete(string CompanyId, string BranchId, string UserName, long[] Arr)
        {
            try
            {
                _unitOfWork.BeginTransaction(IsolationLevel.ReadCommitted);
                
                foreach(var id in Arr)
                {
                    _unitOfWork.Execute("UPDATE M03_Sale_Area SET IsDeleted = 1, UpdatedDate = @Date, UpdatedBy = @UserName " +
                        "WHERE CompanyId = @CompanyId AND BranchId = @BranchId AND Id = @Id", new
                    {
                        Date = DateTime.Now,
                        UserName,
                        CompanyId,
                        BranchId,
                        Id = id
                    });
                }

                return _unitOfWork.CommitTransaction();
            }
            catch (Exception ex)
            {
                _unitOfWork.RollBackTransaction();
                throw ex;
            }
        }

        public int IsCheckDelete(string CompanyId, string BranchId, long[] Arr, out long[] arrOutId)
        {
            int num = 0;
            arrOutId = new long[0];
            IEnumerable<T03_Sales_Invoice_Info> source = _unitOfWork.GenericRepository<T03_Sales_Invoice_Info>().Query(CompanyId, BranchId).Where(t => Arr.Contains(t.AreaId.HasValue ? t.AreaId.Value : 0L));
            if (source.Count() > 0)
            {
                arrOutId = source.Select(p => p.AreaId.Value).Distinct().ToArray();
                num = 9;
            }
            return num;
        }

        public IEnumerable<ComboboxSelectModels> GetDataCodeName(BaseModels model)
        {
            return _unitOfWork.GenericRepository<M03_Sale_Area>().Query(null, model.BranchId, t => t.IsUsed == 1 && t.IsDeleted == 0).Select(t => new ComboboxSelectModels()
            {
                ID = t.Id,
                Code = t.Code,
                Name = t.Name,
                Field3 = t.Printer,
                Field4 = t.IsTinhGio.ToString(),
                Field6 = t.Discount,
                Field7 = t.DiscAmount,
                Field8 = t.Percent_ServiceCharge,
                Field9 = t.ServiceChargeAmount,
                Field10 = t.VAT_DiscAmount
            }).ToArray();
        }


        // lam duy them cai nay
        public int GetLastSaleAreaId()
        {
            return this._unitOfWork.Query<int>("select max(Id) from M03_Sale_Area", new object[]
            {

            }).FirstOrDefault<int>();
        }

        public IEnumerable<M03_Sale_Area> Pagintion(int pageSize, int pageIndex, string companyId, out int statusCode)
        {
            try
            {
                var result = _unitOfWork.Query<M03_Sale_Area> ("Select  * from M03_Sale_Area where CompanyId = @CompanyId ",new object[]
                {
                    new
                    {
                        CompanyId = companyId

                    }
                }).Skip(pageSize * (pageIndex - 1)).Take(pageSize).ToArray();
                if (result != null)
                {
                    statusCode = 0;
                    return result;
                }
                else
                {
                    statusCode = 9;
                    return result;
                }
            }
            catch (Exception ex)
            {
                statusCode = 9;
                return null;
            }
        }
    }
}
