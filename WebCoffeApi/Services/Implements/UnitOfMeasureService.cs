﻿using FasCon.Apis.EntityFrameworks;
using System.Runtime.CompilerServices;
using FasCon.Apis.Models;
using FasCon.Apis.Mapping;
using System.Data;
using System.Linq;

namespace FasCon.Apis.Services
{
    public class UnitOfMeasureService : IUnitOfMeasureService
    {
        public UnitOfMeasureService()
        {
            this._unitOfWork = new UnitOfWork();

        }
        public IEnumerable<UnitOfMeasureModel> GetAll(UnitOfMeasureModel searchModel,out int statusCode)
        {
            statusCode = 9;
            IEnumerable<UnitOfMeasureModel> result = this._unitOfWork.Query<UnitOfMeasureModel>("select * from M04_UnitOfMeasure where CompanyId=@Id ",new object[]
            {
                new
                {
                    Id=searchModel.CompanyId
                }

            }).ToArray<UnitOfMeasureModel>();
            if(result != null)
            {
                statusCode = 0;
                return result;
            }
            return result;


        }

        public M04_UnitOfMeasure Add(UnitOfMeasureModel model, out int statusCode)
        {
            var entity = new M04_UnitOfMeasure
            {
                Id= this.GetLastUnitOfMeasureId() + 1,
                Code = model.Code,
                Name = model.Name,
                Note=model.Note,
                //StatusId = model.StatusId,
                CompanyId = model.CompanyId,//
                IsDeleted =0,//
                CreatedDate= System.DateTime.Now,//
                CreatedBy = model.CreatedBy,//
                UpdatedDate =DateTime.Now,
                UpdatedBy = model.UpdatedBy,
                //ApprovedDate = model.ApprovedDate,
                //ApprovedBy = model.ApprovedBy
            };
            try
            {
                this._unitOfWork.BeginTransaction(IsolationLevel.ReadUncommitted);
                this._unitOfWork.GenericRepository<M04_UnitOfMeasure>().Insert(entity);

                statusCode = this._unitOfWork.CommitTransaction();
                return entity;
            }
            catch (Exception ex)
            {
                
                statusCode = 9;
                this._unitOfWork.RollBackTransaction();
                throw ex;
            }
            return null ;

        }

        public int GetLastUnitOfMeasureId()
        {
            return this._unitOfWork.Query<int>("select max(Id) from M04_UnitOfMeasure", new object[]
            {

            }).FirstOrDefault<int>();   
        }

        public M04_UnitOfMeasure Delete(long id, string updateBy, string companyId, out int statusCode)
        {
            try
            {
                var entity = this.GetMeasureById(id,companyId);
                if (entity != null)
                {
                    if (isUnitOfMeasureExistInItem(id))
                    {
                        entity.IsDeleted = 1;
                        entity.UpdatedBy = updateBy;
                        entity.UpdatedDate= DateTime.Now;
                        this._unitOfWork.BeginTransaction(IsolationLevel.ReadUncommitted);
                        this._unitOfWork.GenericRepository<M04_UnitOfMeasure>().Update(entity);
                        statusCode = this._unitOfWork.CommitTransaction();

                    }
                    else
                    {
                        this._unitOfWork.BeginTransaction(IsolationLevel.ReadUncommitted);
                        this._unitOfWork.GenericRepository<M04_UnitOfMeasure>().Delete(entity);
                        statusCode = this._unitOfWork.CommitTransaction();
                    }
                    return entity;
                   
                }
                else
                {
                    
                    statusCode = 9;
                    return null;
                }

            }
            catch (Exception ex)
            {
                this._unitOfWork.RollBackTransaction();
                statusCode = 9;
                throw ex;
                return null;
            }
            
        }

        public M04_UnitOfMeasure GetMeasureById(long Id,string companyId)
        {
            var measurement= this._unitOfWork.Query<M04_UnitOfMeasure>("select * from M04_UnitOfMeasure where Id = @Id and CompanyId=@CompanyId", new object[]
           {
               new
               {
                   Id=Id,
                   CompanyId=companyId

               }
           }).FirstOrDefault<M04_UnitOfMeasure>();

            return measurement;
        }

        public bool isUnitOfMeasureExistInItem(long id)
        {
           var result = this._unitOfWork.Query<M04_Item>("select * from M04_Item where UnitBasic = @Id", new object[]
           {
               new
               {
                   Id=id
               }
           }).FirstOrDefault<M04_Item>();
            if (result != null)
            {
                return true;
            }
            return false;   
        }

        public M04_UnitOfMeasure GetMeasureById(long id, out int statusCode)
        {
            try
            {
               var result = this._unitOfWork.Query<M04_UnitOfMeasure>("select * from M04_UnitOfMeasure where Id = @Id", new object[]{
               new
               {
                   Id=id
               }}).FirstOrDefault<M04_UnitOfMeasure>();
                if (result != null)
                {
                    statusCode = 0;
                    return result;
                }
                else
                {
                    statusCode = 9;
                    return null;
                }
            }
            catch
            {
                statusCode = 9;
                return null;
            }   
        }

        public IEnumerable<UnitOfMeasureModel> Pagnition(int pageSize, int pageIndex, string companyId, out int statusCode)
        {
            try
            {
                var result = this._unitOfWork.Query<UnitOfMeasureModel>("select * from M04_UnitOfMeasure where CompanyId=@CompanyId ", new object[]
                {
                    new
                    {
                        CompanyId=companyId
                    }
                }).Skip(pageSize * (pageIndex-1)).Take(pageSize).ToArray<UnitOfMeasureModel>();
                if (result != null)
                {
                    statusCode = 0;
                    return result;
                }
                else
                {
                    statusCode = 9;
                    return null;
                }   

            }
            catch
            {

                statusCode = 9;
                return null;

            }
        }

        public M04_UnitOfMeasure Update(UnitOfMeasureModel model, out int statusCode)
        {
            try
            {
                var entity = this.GetMeasureById(model.Id, model.CompanyId);
                if (entity != null)
                {
                    entity.Code = model.Code;
                    entity.Name = model.Name;
                    entity.Note = model.Note;
                    entity.UpdatedBy = model.UpdatedBy;
                    entity.UpdatedDate = DateTime.Now;
                    this._unitOfWork.BeginTransaction(IsolationLevel.ReadUncommitted);
                    this._unitOfWork.GenericRepository<M04_UnitOfMeasure>().Update(entity);
                    statusCode = this._unitOfWork.CommitTransaction();
                }
                else
                {
                    statusCode = 9;
                }
                return entity;

            }
            catch
            {
                this._unitOfWork.RollBackTransaction();
                statusCode = 9;
                return null;
            }
        }

        public IEnumerable<M04_UnitOfMeasure> SearchByCode(string codeName, string companyId, out int statusCode)
        {
            statusCode = 0;
            try
            {
                var result = this._unitOfWork.Query<M04_UnitOfMeasure>("select * from M04_UnitOfMeasure WHERE CompanyId = @CompanyId AND Code LIKE @CodeName", new object[]
                {
                    new
                    {
                        CompanyId=companyId,
                        CodeName="%" + codeName + "%"
                    }
                }).ToArray<M04_UnitOfMeasure>();
                return result;
            }
            catch
            {
                
                statusCode = 9;
                return null;
            }
        }

        private UnitOfWork _unitOfWork;
    }
}
