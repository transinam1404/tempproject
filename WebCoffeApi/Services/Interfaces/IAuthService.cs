using System;
using System.Collections.Generic;
using FasCon.Apis.Models;

namespace FasCon.Apis.Services
{
	 
	public interface IAuthService
	{
	 
		UserLoginModels CheckLogin(UserLoginModels model, out int statusCode);

	 
		IEnumerable<RoleModels> GetListRoleByUser(RoleModels model);
	}
}
