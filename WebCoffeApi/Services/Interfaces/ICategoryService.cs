﻿using FasCon.Apis.Mapping;
using FasCon.Apis.Models;

namespace FasCon.Apis.Services
{
	public interface ICategoryService
	{
        IEnumerable<CategoryModel> GetAll(CategoryModel searchModel, out int statusCode);
        int GetLastCategoryId();
        M04_Item_Category Add(CategoryModel model, out int statusCode);
        M04_Item_Category Delete(long id, string updateBy, string companyId, out int statusCode);
        M04_Item_Category GetCategoryById(long id, string companyId);
        M04_Item_Category GetCategoryById(long id, out int statusCode);
        bool isCategoryExistInItem(long id);
        M04_Item_Category Update(CategoryModel model, out int statusCode);
        IEnumerable<M04_Item_Category> SearchByCode(string codeName, string companyId, out int statusCode);

        IEnumerable<CategoryModel> Pagnition(int pageSize, int pageIndex, string companyId, out int statusCode);
    }
}

