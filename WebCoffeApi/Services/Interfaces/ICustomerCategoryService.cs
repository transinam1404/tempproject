﻿using FasCon.Apis.Mapping;
using FasCon.Apis.Models;
namespace FasCon.Apis.Services
{
    public interface ICustomerCategoryService
    {
        IEnumerable<CustomerCategoryModels> GetAll(CustomerCategoryModels searchModel, out int statusCode);
        int GetLastCustomerId();
        void Add(CustomerCategoryModels model, out int statusCode);
        void Delete(long id, string updateBy, string companyId, out int statusCode);
        M03_Customer_Category GetCustomerCategoryById(long id, string companyId);
        bool isUnitOfCustomerCategoryExistInItem(long id);
        M03_Customer_Category GetCustomerCategoryById(long id, out int statusCode);
        void Update(CustomerCategoryModels model, out int statusCode);
        IEnumerable<CustomerCategoryModels> Pagnition(int pageSize, int pageIndex, string companyId, out int statusCode);
        IEnumerable<M03_Customer_Category> SearchByCode(string codeName, string companyId, out int statusCode);



    }
}
