﻿
// Type: FasCon.Apis.Services.ISaleAreaService




using FasCon.Apis.Mapping;
using FasCon.Apis.Models;
using System.Collections.Generic;
using WebCoffeApi.Models;


namespace FasCon.Apis.Services
{
    public interface ISaleAreaService
  {
    IEnumerable<SaleAreaModels> GetList(SaleAreaModels modeSearch , out int statusCode  );

    IEnumerable<ComboboxSelectModels> GetDataCodeName(BaseModels model);

    SaleAreaModels SelectByID(SaleAreaModels model);

    bool IsCodeExists(string BranchID, string Code);

    SaleAreaModels Insert(SaleAreaModels model);

    SaleAreaModels Update(SaleAreaModels model);

    int Delete(string CompanyId, string BranchId, string UserName, long[] Arr);
    M03_Sale_Area Delete(string CompanyId, string BranchId, string UserName, long Id, out int statusCode);


    int KiemTraTonTaiID(string CompanyId, string BranchId, long id);

    int IsCheckDelete(string CompanyId, string BranchId, long[] Arr, out long[] arrOutId);

    IEnumerable<M03_Sale_Area> Pagintion(int pageSize, int pageIndex, string companyId, out int statusCode);
  }
}
