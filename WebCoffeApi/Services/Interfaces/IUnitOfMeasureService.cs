﻿
using FasCon.Apis.Mapping;
using FasCon.Apis.Models;

namespace FasCon.Apis.Services
{
    public interface IUnitOfMeasureService
    {
        IEnumerable<UnitOfMeasureModel> GetAll(UnitOfMeasureModel searchModel,out int statusCode);
        int GetLastUnitOfMeasureId();
        M04_UnitOfMeasure Add(UnitOfMeasureModel model, out int statusCode);
        M04_UnitOfMeasure Delete(long id,string updateBy,string companyId, out int statusCode);
        M04_UnitOfMeasure GetMeasureById(long id,string companyId);
        M04_UnitOfMeasure GetMeasureById(long id,out int statusCode);
        bool isUnitOfMeasureExistInItem(long id);
        M04_UnitOfMeasure Update(UnitOfMeasureModel model, out int statusCode);
        IEnumerable<M04_UnitOfMeasure> SearchByCode(string codeName, string companyId, out int statusCode);

        IEnumerable<UnitOfMeasureModel> Pagnition(int pageSize, int pageIndex, string companyId, out int statusCode);
    }
}
